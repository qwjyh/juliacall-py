from juliacall import Main as jl

def hello_from_julia():
    jl.println("Hello from Julia")

jl.seval("using SpecialFunctions")

def print_Gamma(x: float | int):
    gamma_4 = jl.gamma(x)
    print(f"Γ(4) = {gamma_4}")

